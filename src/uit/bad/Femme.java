/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uit.bad;

/**
 *
 * @author raya
 */
public class Femme extends Humain {
    private String nom;
    private String prenom;
    private int age;
    
    public Femme() {
    }
    
    public Femme(String nom, String prenom, int age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }
    
    @Override
    public String toString() {
        return "Femme: " + super.toString(); 
    }
    
    public static void main(String[] args) {
        Femme femme = new Femme("Doe", "Alice", 30);
        Homme homme = new Homme("Smith", "John", 35);
        
        femme.ami(homme);
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public String getPrenom() {
        return prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
}
